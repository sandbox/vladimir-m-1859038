
ATTENTION
=========
*** THIS MODULE CAN BRAKE YOUR DATABASE. PLEASE BEFORE INSTALL THIS MODULE BACKUP YOUR DATABASE***


INSTALLATION
=============
- Copy module to your environment;
- Enable module "/admin/modules";
- Setup permission "/admin/people/permissions";
- Configure module "/admin/config/development/find_replace"

CONFIGURATION
==============
- "Settings > Settings" define the name of the tables where symbol search will be implemented.
  Please start the name of the table in a new row.
  For example:
    field_data_body
    field_revision_body
    node
    node_revision
    etc.

- "Settings > Characters" define the needle strings with a replaced string delimiting it with |||.
  Each condition has to start in a new row.
  For example [needle]|||[new string]:
    â€œ|||".

PROCESS
=======
- Click on "Process" button
- Clear the cache.