<?php
/**
 * @file
 *  Administer module
 */

/**
 * Implementation of hook_form()
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function find_replace_form($form, &$form_state) {
  $form = array();
  $weight = 1;

  $t = array(
    'field_data_body',
    'field_revision_body',
    'node',
    'node_revision',
  );

  $c = array(
    'â€¦|||...',
    'â€“|||-',
    "â€™|||'",
    'â€œ|||\"',
    'â€|||\"',
    'Â®|||&reg;',
  );

  $tables_values = variable_get('find_replace_process_characters_tables');
  if (isset($tables_values)) {
    $default_tables = $tables_values;
  }
  else {
    $default_tables = implode("\n", $t);
  }

  $chars_values = variable_get('find_replace_process_characters_characters');
  if (!empty($chars_values)) {
    $default_characters = variable_get('find_replace_process_characters_characters');
  }
  else {
    $default_characters = implode("\n", $c);
  }

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#weight' => $weight++,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['tables'] = array(
    '#type' => 'textarea',
    '#title' => t('Tables'),
    '#default_value' => $default_tables,
    '#description' => t("Insert the name of the tables where symbol search will be implemented.
      Please start the name of the table in a new row. For example:
      <em>field_data_body field_revision_body node node_revision etc.</em>"
    ),
    '#required' => TRUE,
    '#weight' => $weight++,
  );

  $form['settings']['characters'] = array(
    '#type' => 'textarea',
    '#title' => t('Characters'),
    '#description' => t('Fill in the needle strings with a replaced string delimiting it with |||.
      Each condition has to start in a new row. For example [needle]|||[new string]:<em> â€œ|||".</em>'
    ),
    '#default_value' => $default_characters,
    '#required' => TRUE,
    '#weight' => $weight++,
  );

  $form['settings']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Process'),
    '#weight' => $weight++,
  );

  return $form;
}

/**
 * Implementation of find_replace_process_characters_form() submit callback.
 *
 * @see find_replace_process_characters_form()
 * @param $form
 * @param $form_state
 */
function find_replace_form_submit($form, &$form_state) {
  variable_set('find_replace_process_characters_tables', $form_state['values']['tables']);
  variable_set('find_replace_process_characters_characters', $form_state['values']['characters']);

  $tables = explode("\n", $form_state['values']['tables']);
  $characters_values = explode("\n", $form_state['values']['characters']);

  $characters = array();
  foreach ($characters_values as $chars_data) {
    list($key, $val) = explode('|||', $chars_data);
    $characters[$key] = $val;
  }

  if (!empty($tables) && !empty($characters)) {
    $primary_fields = array();
    foreach ($tables as $table) {
      trim($table);

      $table_fields = _find_replace_get_table_field_names($table);

      if (!empty($table_fields)) {
        // detect primary fields
        foreach ($table_fields as $table_field) {
          if ($table_field->Key == 'PRI') {
            if (!in_array($table_field->Field, $primary_fields)) {
              $primary_fields[] = $table_field->Field;
            }
          }
        }
        foreach ($table_fields as $table_field) {
          foreach ($characters as $char_key => $char_value) {
            $content = db_query("SELECT * FROM {$table} WHERE CONVERT({$table_field->Field} USING utf8 ) LIKE  '%{$char_key}%'");
            if (!empty($content)) {
              while ($row = $content->fetchObject()) {
                if (!empty($row)) {
                  foreach ($row as $field => $value) {
                    if (in_array($field, $primary_fields)) {
                      $conditions[$field] = $value;
                    }
                  }

                  $initial_content = $row->{$table_field->Field};

                  // process serialized values
                  if (_find_replace_is_serial($initial_content)) {
                    // $sc = serialized content
                    $sc = unserialize($initial_content);
                    foreach ($sc as $sc_key => $sc_value) {
                      // check if chars, string
                      if (is_string($sc_value)) {
                        if (strpos($sc_value, trim($char_key))) {
                          $sc[$sc_key] = str_replace(trim($char_key), trim($char_value), $sc_value);
                          ;
                        }
                        $temp_str = serialize($sc);
                      }
                    }

                    $result = db_update($table)->fields(array($table_field->Field => $temp_str));

                    if (!empty($conditions)) {
                      foreach ($conditions as $key => $value) {
                        $result->condition($key, $value, '=');
                      }
                    }
                    $result->execute();

                    if ($result) {
                      drupal_set_message(t('Field %name from table %table was processed.', array('%name' => $table_field->Field, '%table' => $table)));
                    }

                    $conditions = array();
                  }
                  else {
                    // process another type
                    $temp_str = str_replace(trim($char_key), trim($char_value), $initial_content);

                    $result = db_update($table)->fields(array($table_field->Field => $temp_str));

                    if (!empty($conditions)) {
                      foreach ($conditions as $key => $value) {
                        $result->condition($key, $value, '=');
                      }
                    }
                    $result->execute();

                    if ($result) {
                      drupal_set_message(t('Field %name from table %table was processed.', array('%name' => $table_field->Field, '%table' => $table)));
                    }

                    $conditions = array();
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  drupal_set_message(t('Done.'));
  drupal_set_message(t('To apply changes please make') . ' ' . l(t('Clear cache'), 'admin/config/development/performance'));
}

/**
 * Get table columns
 *
 * @param $table
 * @return mixed
 */
function _find_replace_get_table_field_names($table) {
  $field_names = array();
  if (isset($table)) {
    if (db_table_exists($table)) {
      return db_query("SHOW COLUMNS FROM {$table}")->fetchAll();
    }
    else {
      drupal_set_message(t('Table or view: %table, not found.', array('%table' => $table)), 'error');
    }
  }
}

/**
 * Check if a string is serialized
 * $url http://stackoverflow.com/questions/1369936/check-to-see-if-a-string-is-serialized
 *
 * @param string $string
 */
function _find_replace_is_serial($string) {
  return (@unserialize($string) !== false);
}